import logging

from google.cloud import bigquery
from pandas import DataFrame

import credentials


class BigQuery:

	def __init__(self):
		self.client = bigquery.Client()

	def tables(self, dataset):
		ds_list = self.client.list_tables(dataset)
		result = DataFrame(dict(
			tab_name=[table.table_id for table in ds_list]
		))
		return result

	def cost(self, sql, verbose=True):
		job_config = bigquery.QueryJobConfig()
		job_config.dry_run = True
		job_config.use_query_cache = False
		job = self.client.query(sql, location='US', job_config=job_config)
		mb = job.total_bytes_processed / 1024**2
		dollar = (job.total_bytes_processed / 1024**3) * 0.02
		if verbose:
			logger = logging.getLogger('pyfrost')
			logger.info(f'{mb:2,.0f} MB, US$ {dollar:,.2f}')
		return dollar

	def head(self, table, nrows=5):
		table = self.client.get_table(table)
		return self.client.list_rows(table, max_results=nrows).to_dataframe()

	def partitions(self, table):
		table = self.client.get_table(table)
		return self.client.list_partitions(table)

	def query(self, sql, verbose=True):
		job = self.client.query(sql)
		result = job.result().to_dataframe()
		if verbose and job.total_bytes_billed is not None:
			mb = job.total_bytes_billed / 1024**2
			dollar = job.total_bytes_billed / 1024**3 * 0.02
			logger = logging.getLogger('pyfrost')
			logger.info(f'{mb:2,.0f} MB, US$ {dollar:,.2f}')
		return result

	def download(self, table):
		table = self.client.get_table(table)
		return self.client.list_rows(table).to_dataframe()

	def upload(self, data, table_name):
		self.client.load_table_from_dataframe(data, table_name)
